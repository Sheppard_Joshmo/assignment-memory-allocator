#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H


#include "mem.h"
#include "mem_internals.h"
# define MAP_ANONYMOUS	0x20		/* Don't use a file.  */
# define MAP_FIXED_NOREPLACE 0x100000	/* MAP_FIXED but do not unmap */

void test1(void);

void test2(void);

void test3(void);

void test4(void);

void test5(void);

#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
