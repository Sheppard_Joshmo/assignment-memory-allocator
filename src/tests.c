#include "tests.h"
#include <stdlib.h>

#define KILOBYTE 1024

struct block_header *block_get_header(void *contents);

static void print_and_debug_heap(const void *const heap, const char *const msg) {
    printf("%s: %s", msg, "\n");
    debug_heap(stdout, heap);
}

void fail(const char *msg) {
    fprintf(stderr, "test failed. %s%s", msg, "\n");
    abort();
}

void success(const char *test_name, const char *msg) {
    fprintf(stdout, "%s%s%s", test_name, msg, "\n");
}

static void done(void *heap, int query, const char *const test_name) {
    munmap(heap, size_from_capacity((block_capacity) {.bytes = query}).bytes);
    success(test_name, " is successful!");
}

/* Обычное успешное выделение памяти. */
void test1(void) {
    void *test_heap = heap_init(4 * KILOBYTE);
    if (!test_heap) fail("Heap is not initialized.");
    print_and_debug_heap(test_heap, "Heap after init");
    void *mem = _malloc(2 * KILOBYTE);
    if (!mem) fail("Memory is not allocated.");
    print_and_debug_heap(test_heap, "Heap after alloc");
    _free(mem);
    print_and_debug_heap(test_heap, "Heap after free mem");
    done(test_heap, 4 * KILOBYTE, "Test 1");
}

/* Освобождение одного блока из нескольких выделенных. */
void test2(void) {
    void *test_heap = heap_init(4 * KILOBYTE);
    if (!test_heap) fail("Heap is not initialized.");
    print_and_debug_heap(test_heap, "Heap after init");
    void *mem1 = _malloc(KILOBYTE);
    void *mem2 = _malloc(KILOBYTE);
    if (!mem1 || !mem2) fail("Memory is not allocated.");
    print_and_debug_heap(test_heap, "Heap after alloc");
    _free(mem1);
    if (!mem2) fail("Release of the first damaged the second.");
    print_and_debug_heap(test_heap, "Heap after free mem");
    _free(mem2);
    done(test_heap, 4 * KILOBYTE, "Test 2");
}

/* Освобождение двух блоков из нескольких выделенных. */
void test3(void) {
    void *test_heap = heap_init(4 * KILOBYTE);
    if (!test_heap) fail("Heap is not initialized.");
    print_and_debug_heap(test_heap, "Heap after init");
    void *mem1 = _malloc(KILOBYTE);
    void *mem2 = _malloc(KILOBYTE);
    void *mem3 = _malloc(KILOBYTE);
    if (!mem1) fail("First memory is not allocated.");
    if (!mem2 || !mem3) fail(" Second memory is not allocated.");
    if (!mem3) fail("Third memory is not allocated.");
    print_and_debug_heap(test_heap, "Heap after alloc");
    _free(mem1);
    _free(mem3);
    if (!mem2) fail("Release of the first damaged the second.");
    print_and_debug_heap(test_heap, "Heap after free mem");
    done(test_heap, 4 * KILOBYTE, "Test 3");
}

/* Память закончилась, новый регион памяти расширяет старый. */
void test4(void) {
    void *test_heap = heap_init(4 * KILOBYTE);
    if (!test_heap) fail("Heap is not initialized.");
    print_and_debug_heap(test_heap, "Heap after init");
    void *mem1 = _malloc(8 * KILOBYTE);
    void *mem2 = _malloc(16 * KILOBYTE);
    if (!mem1) fail("First memory is not allocated.");
    if (!mem2) fail("Second memory is not allocated.");
    print_and_debug_heap(test_heap, "Heap after alloc");
    struct block_header *header1 = block_get_header(mem1);
    struct block_header *header2 = block_get_header(mem2);
    if (!header1 || header1->next != header2) fail("Headers are not linked.");
    done(test_heap, (int) (header1->capacity.bytes + header2->capacity.bytes +
                           ((struct block_header *) test_heap)->capacity.bytes), "Test 4");
}

/* Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов,
    * новый регион выделяется в другом месте. */
void test5(void) {
    void *test_heap = heap_init(4 * KILOBYTE);
    if (!test_heap) fail("Heap is not initialized.");
    print_and_debug_heap(test_heap, "Heap after init");
    void *mem1 = _malloc(8 * KILOBYTE);
    if (!mem1) fail("First memory is not allocated.");
    struct block_header *header1 = block_get_header(mem1);
    if (!header1) fail("Header is not find.");
    print_and_debug_heap(test_heap, "Heap after alloc.");
    void *region = mmap(header1->contents + header1->capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE,
                        MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);
    void *mem2 = _malloc(8 * KILOBYTE);
    struct block_header *header2 = block_get_header(mem2);
    if (!mem2) fail("Second memory is not allocated");
    print_and_debug_heap(test_heap, "Heap after second malloc");
    _free(mem1);
    _free(mem2);
    print_and_debug_heap(test_heap, "Heap after realising");
    munmap(region, size_from_capacity((block_capacity) {.bytes = REGION_MIN_SIZE}).bytes);
    done(test_heap, (int) (header1->capacity.bytes + header2->capacity.bytes +
                           ((struct block_header *) test_heap)->capacity.bytes), "Test 5");
}
